#version 430

uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

uniform float treeH;
uniform float treeR;

layout(location = 0) in vec3 position;

flat out int visible;

void main()
{
    gl_Position = vec4(position, 1.0);
    vec3 center = position + vec3(0, 0, treeH / 2);
    vec4 pos;

    int outOfBound[6] = int[6]( 0, 0, 0, 0, 0, 0 );

    for( int i = -1; i < 2; i += 2 ) {
        for( int j = -1; j < 2; j += 2 ) {
            for( int k = -1; k < 2; k += 2 ) {
                pos = projectionMatrix * viewMatrix *
                    vec4(center + vec3(treeR * i, treeR * j, treeH * k / 2), 1.0);
                pos = pos / pos.w;
                for( int l = 0; l < 2; ++l ) {
                    if( pos[l] < -1 ) {
                        outOfBound[l] += 1;
                    }
                    if( pos[l] > 1 ) {
                        outOfBound[2 + l] += 1;
                    }
                }
                if( pos[2] < 0 ) {
                    outOfBound[4] += 1;
                }
                if( pos[2] > 1 ) {
                    outOfBound[5] += 1;
                }
            }
        }
    }

    bool inFrustum = true;
    for( int i = 0; i < 6; i++ ) {
        if ( outOfBound[i] == 8 ) {
            inFrustum = false;
        }
    }

    visible = inFrustum ? 1 : 0;
}
