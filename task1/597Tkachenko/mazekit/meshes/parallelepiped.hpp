#pragma once


#include "Mesh.hpp"


namespace mazekit {
namespace meshes {

MeshPtr makeParallelepiped(float length, float width, float height);

}
}