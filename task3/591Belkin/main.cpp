#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>

#include <vector>
#include <iostream>
#include <queue>

#include <glm/gtc/constants.hpp>


void addCone(float height, float radius, int nPolygones, glm::mat4 modelMatrix,
        std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texcoords)
{
    glm::vec3 coneTop = glm::vec3(0.0f, 0.0f, height);

    for(int i = 0; i < nPolygones; ++i) {
        float angle1 = glm::two_pi<float>() * i / nPolygones;
        glm::vec3 v1 = radius * glm::vec3(glm::cos(angle1), glm::sin(angle1), 0.0f);

        float angle2 = glm::two_pi<float>() * (i + 1) / nPolygones;
        glm::vec3 v2 = radius * glm::vec3(glm::cos(angle2), glm::sin(angle2), 0.0f);

        // боковая поверхность
        vertices.push_back(v1);
        vertices.push_back(v2);
        vertices.push_back(coneTop);

        normals.push_back(glm::normalize(v1));
        normals.push_back(glm::normalize(v2));
        normals.push_back(glm::normalize(coneTop));

        texcoords.push_back(glm::vec2((0.0 + i) / nPolygones, 0));
        texcoords.push_back(glm::vec2((1.0 + i) / nPolygones, 0));
        texcoords.push_back(glm::vec2((0.5 + i) / nPolygones, 10));

        // основание
        vertices.push_back(v1);
        vertices.push_back(v2);
        vertices.emplace_back(glm::vec3(0.0f, 0.0f, 0.0f));

        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));

        texcoords.push_back(glm::vec2(0, 0));
        texcoords.push_back(glm::vec2(0, 0));
        texcoords.push_back(glm::vec2(0, 0));
    }

    for( int i = vertices.size() - nPolygones * 6; i < vertices.size(); ++i ) {
        vertices[i] = modelMatrix * glm::vec4(vertices[i], 1.0);
        normals[i] = modelMatrix * glm::vec4(normals[i], 1.0);
    }
}

void addRect(float width, float height, float textureScale, glm::mat4 modelMatrix,
        std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texcoords)
{
    //front 1
    vertices.push_back(glm::vec3(-width / 2, height / 2, 0.0));
    vertices.push_back(glm::vec3(width / 2, -height / 2, 0.0));
    vertices.push_back(glm::vec3(width / 2, height / 2, 0.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(0, textureScale));
    texcoords.push_back(glm::vec2(textureScale, 0));
    texcoords.push_back(glm::vec2(textureScale, textureScale));

    //front 2
    vertices.push_back(glm::vec3(-width / 2, height / 2, 0.0));
    vertices.push_back(glm::vec3(-width / 2, -height / 2, 0.0));
    vertices.push_back(glm::vec3(width / 2, -height / 2, 0.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(0, textureScale));
    texcoords.push_back(glm::vec2(0, 0));
    texcoords.push_back(glm::vec2(textureScale, 0));

    for( int i = vertices.size() - 6; i < vertices.size(); ++i ) {
        vertices[i] = modelMatrix * glm::vec4(vertices[i], 1.0);
        normals[i] = modelMatrix * glm::vec4(normals[i], 1.0);
    }
}

MeshPtr createMesh(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texcoords)
{
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Mesh created with " << vertices.size() << " vertices\n";

    return mesh;
}

MeshPtr makeCone(float height, float radius, int nPolygones)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    addCone(height, radius, nPolygones, glm::mat4(1.0), vertices, normals, texcoords);

    return createMesh(vertices, normals, texcoords);
}

MeshPtr makeRect(float width, float height, float textureScale=1.0)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    addRect(width, height, textureScale, glm::mat4(1.0), vertices, normals, texcoords);

    return createMesh(vertices, normals, texcoords);
}


class TreeBuilder {
public:

    void Build( MeshPtr& trunk, MeshPtr& leafs, float& height, glm::mat4 modelMatrix = glm::mat4( 1.0f ) )
    {
        trunkVertices.clear();
        trunkNormals.clear();
        trunkTexcoords.clear();

        leafsVertices.clear();
        leafsNormals.clear();
        leafsTexcoords.clear();

        build( 1, 0.1, 3, modelMatrix );

        height = 0;
        for( glm::vec3 v : leafsVertices ) {
            if( height < v[2] ) {
                height = v[2];
            }
        }

        trunk = createMesh(trunkVertices, trunkNormals, trunkTexcoords);
        leafs = createMesh(leafsVertices, leafsNormals, leafsTexcoords);

        trunk->setModelMatrix(glm::mat4(1.0));
        leafs->setModelMatrix(glm::mat4(1.0));
    }

private:
    std::vector<glm::vec3> trunkVertices;
    std::vector<glm::vec3> trunkNormals;
    std::vector<glm::vec2> trunkTexcoords;

    std::vector<glm::vec3> leafsVertices;
    std::vector<glm::vec3> leafsNormals;
    std::vector<glm::vec2> leafsTexcoords;

    void build( int depth, float radius, float length, glm::mat4 mat )
    {
        if( depth > 3 && depth <= 6 ) {
            int n_leafs = 10 + rand() % 10;
            for( int i = 0; i < n_leafs; ++i ) {
                float width = 0.03;
                float height = 0.05;
                float displacement = length * ( 0.3f + 1.0f * (rand() % 70) / 100);
                float distance = radius * ( 1.0f + (rand() % 200) / 100 );
                float alpha = glm::radians( 1.0f * (rand() % 360) );
                float beta = glm::radians( -30.0f + rand() % 100 );

                glm::mat4 leafMat = glm::rotate( glm::mat4(1), alpha, glm::vec3( 0.0f, 0.0f, 1.0f ) );
                leafMat = glm::translate( leafMat, glm::vec3( distance, 0, displacement ));
                leafMat = glm::rotate( leafMat, beta, glm::vec3( 1.0f, 0.0f, 0.0f ) );
                leafMat = glm::translate( leafMat, glm::vec3( 0, height / 2, 0 ));

                addRect(width, height, 1.0, mat * leafMat, leafsVertices, leafsNormals, leafsTexcoords);
            }
        } else if( depth > 5 ) {
            return;
        }

        int n = 100 / (depth * glm::log(1 + depth) );

        addCone(length, radius, n, mat, trunkVertices, trunkNormals, trunkTexcoords);

        for( int level = 1; level <= 3; ++level ) {
            int n_children = 3 + rand() % 3;
            for( int i = 0; i < n_children; ++i ) {
                float h = length * ( 0.15f + 0.2f * level + 1.0f * (rand() % 20) / 100 );
                float r = radius * ( 1.0f - h / length) * (0.9f - 1.0f * (rand() % 20) / 100 );
                float l = length * ( 0.7f - 1.0f * (rand() % 20) / 100 ) / level;
                float alpha = glm::radians( 360.0f * i / n_children + rand() % 40 );
                float beta = glm::radians( 30.0f + rand() % 10 );

                glm::mat4 childMat = glm::translate( glm::mat4( 1.0f ), glm::vec3( 0.0f, 0.0f, h ) );
                childMat = glm::rotate( childMat, alpha, glm::vec3( 0.0f, 0.0f, 1.0f ) );
                childMat = glm::rotate( childMat, beta, glm::vec3( 1.0f, 0.0f, 0.0f ) );

                build( depth + level, r, l, mat * childMat );
            }
        }
    }
};


class TreeApplication : public Application {
public:
    MeshPtr _trunk;
    MeshPtr _leafs;
    MeshPtr _grass;

    std::vector<glm::vec3> treeModelPos;
    DataBufferPtr treeModelPosBuf;
    float groundSizeA = 100;
    float groundSizeB = 100;
    int nTrees;
    float prob = 0.2;
    float treeRadius = 1.0;
    float treeHeight;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _shaderInstanced;
    ShaderProgramPtr _cullShader;

    TexturePtr _barkTexture;
    TexturePtr _leafTexture;
    TexturePtr _grassTexture;

    TexturePtr _bufferTexAll;
    TexturePtr _bufferTexCulled;

    GLuint _sampler;

    //Переменные для управления положением одного источника света
    float _lr = 10.0;
    float _phi = glm::pi<float>() * 0.5f;
    float _theta = glm::pi<float>() * 0.25f;

    LightInfo _light;

    GLuint _TF; //Объект для хранения настроект Transform Feedback
    GLuint _cullVao; //VAO, который хранит настройки буфера для чтения данных во время Transform Feedback
    GLuint _tfOutputVbo; //VBO, в который будут записываться смещения моделей после отсечения

    GLuint _query; //Переменная-счетчик, куда будет записываться количество пройденных отбор моделей

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        TreeBuilder().Build( _trunk, _leafs, treeHeight );

        nTrees = 0;
        int A = groundSizeA / (treeRadius * 2);
        int B = groundSizeB / (treeRadius * 2);
        for( int i = 0; i < A; ++i ) {
            for( int j = 0; j < B; ++j ) {
                if( 1.0f * (rand() % 100) / 100 < prob ) {
                    float a = treeRadius * 2 * i + treeRadius - groundSizeA / 2 + 0.5f * treeRadius * (rand() % 100) / 100;
                    float b = treeRadius * 2 * j + treeRadius - groundSizeB / 2 + 0.5f * treeRadius * (rand() % 100) / 100;

                    treeModelPos.push_back(glm::vec3(a, b, 0.0));
                    nTrees += 1;
                }
            }
        }

        treeModelPosBuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        treeModelPosBuf->setData(nTrees * sizeof(float) * 3, treeModelPos.data());

        //Инициализируем буфер, в который будут скопированы смещения моделей после отсечения
        glGenBuffers(1, &_tfOutputVbo);
        glBindBuffer(GL_ARRAY_BUFFER, _tfOutputVbo);
        glBufferData(GL_ARRAY_BUFFER, nTrees * sizeof(float) * 3, 0, GL_STREAM_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        //Создаем текстурный буфер и привязываем к нему буфер без выравнивания
        _bufferTexAll = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
        _bufferTexAll->bind();
        glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, treeModelPosBuf->id());
        _bufferTexAll->unbind();

        //Создаем текстурный буфер и привязываем к нему буфер без выравнивания
        _bufferTexCulled = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
        _bufferTexCulled->bind();
        glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, _tfOutputVbo);
        _bufferTexCulled->unbind();

        _grass = makeRect( groundSizeA, groundSizeB, 100 );
        _grass->setModelMatrix( glm::mat4( 1 ) );

        _shader = std::make_shared<ShaderProgram>("591BelkinData/shader.vert", "591BelkinData/shader.frag");
        _shaderInstanced = std::make_shared<ShaderProgram>("591BelkinData/instancingTexture.vert", "591BelkinData/shader.frag");

        //----------------------------

        _cullShader = std::make_shared<ShaderProgram>();

        ShaderPtr vs = std::make_shared<Shader>(GL_VERTEX_SHADER);
        vs->createFromFile("591BelkinData/cull.vert");
        _cullShader->attachShader(vs);

        ShaderPtr gs = std::make_shared<Shader>(GL_GEOMETRY_SHADER);
        gs->createFromFile("591BelkinData/cull.geom");
        _cullShader->attachShader(gs);

        //Выходные переменные, которые будут записаны в буфер
        const char* attribs[] = { "position" };
        glTransformFeedbackVaryings(_cullShader->id(), 1, attribs, GL_SEPARATE_ATTRIBS);

        _cullShader->linkProgram();

        //----------------------------

        //VAO, который будет поставлять данные для отсечения
        glGenVertexArrays(1, &_cullVao);
        glBindVertexArray(_cullVao);

        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, treeModelPosBuf->id());
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindVertexArray(0);

        //----------------------------

        //Настроечный объект Transform Feedback
        glGenTransformFeedbacks(1, &_TF);
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, _tfOutputVbo);
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

        //----------------------------

        glGenQueries(1, &_query);

        _leafTexture = loadTexture("591BelkinData/leaf.png");
        _barkTexture = loadTexture("591BelkinData/bark.jpg");
        _grassTexture = loadTexture("591BelkinData/grass.jpg");

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.9, 0.9, 0.9);
        _light.specular = glm::vec3(0.05, 0.05, 0.05);

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        drawGrass();

        cull(_cullShader);
        drawTrees();
    }

    void drawGrass()
    {
        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _grassTexture->bind();
        _shader->setIntUniform("diffuseTex", 0);

        _shader->setMat4Uniform("modelMatrix", _grass->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _grass->modelMatrix()))));
        _grass->draw();

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void drawTrees()
    {
        GLuint primitivesWritten;
        glGetQueryObjectuiv(_query, GL_QUERY_RESULT, &primitivesWritten);

        //Устанавливаем шейдер.
        _shaderInstanced->use();

        //Устанавливаем общие юниформ-переменные
        _shaderInstanced->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shaderInstanced->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shaderInstanced->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shaderInstanced->setVec3Uniform("light.La", _light.ambient);
        _shaderInstanced->setVec3Uniform("light.Ld", _light.diffuse);
        _shaderInstanced->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE1);
        _bufferTexCulled->bind();
        _shaderInstanced->setIntUniform("texBuf", 1);

        glEnable(GL_BLEND);
        glBlendColor(0.7, 0.7, 0.7, 0.7);
        //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glBlendFunc(GL_CONSTANT_COLOR, GL_ONE_MINUS_CONSTANT_COLOR);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _leafTexture->bind();
        _shaderInstanced->setIntUniform("diffuseTex", 0);

        _shaderInstanced->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix))));
        _leafs->drawInstanced(primitivesWritten);

        glDisable(GL_BLEND);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _barkTexture->bind();
        _shaderInstanced->setIntUniform("diffuseTex", 0);

        _shaderInstanced->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix))));
        _trunk->drawInstanced(primitivesWritten);

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void cull(const ShaderProgramPtr& shader)
    {
        shader->use();

        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        shader->setFloatUniform("treeH", treeHeight);
        shader->setFloatUniform("treeR", treeRadius);

        glEnable(GL_RASTERIZER_DISCARD);

        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);

        glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, _query);

        glBeginTransformFeedback(GL_POINTS);

        glBindVertexArray(_cullVao);
        glDrawArrays(GL_POINTS, 0, nTrees);

        glEndTransformFeedback();

        glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

        glDisable(GL_RASTERIZER_DISCARD);
    }
};

int main()
{
    srand( 42 );
    TreeApplication app;
    app.start();

    return 0;
}
