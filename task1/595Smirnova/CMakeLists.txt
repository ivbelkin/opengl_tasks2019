set(SRC_FILES
    application.cpp
    perlin_noise.h
    perlin_noise.cpp
    common/Application.hpp
    common/Application.cpp
    common/ShaderProgram.hpp
    common/ShaderProgram.cpp
    common/Mesh.hpp
    common/Mesh.cpp
    common/Camera.hpp
    common/Camera.cpp
    common/Common.h
    common/DebugOutput.h
    common/DebugOutput.cpp
)

add_definitions(-DGLM_ENABLE_EXPERIMENTAL)

MAKE_OPENGL_TASK(595Smirnova 1 "${SRC_FILES}")
