#include <Mesh.hpp>


/**
 * создаем полигонную модель тора
 * @param R расстояние от центра образующей окружности до оси вращения
 * @param r радиус образующей окружности
 * @param N степень детализации (чем больше, тем детализированнее)
 * @return
 */
MeshPtr makeTorus(float R, float r, unsigned int N);
