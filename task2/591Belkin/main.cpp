#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>

#include <vector>
#include <iostream>
#include <queue>

#include <glm/gtc/constants.hpp>


MeshPtr makeCone(float height, float radius, int nPolygones)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    glm::vec3 coneTop = glm::vec3(0.0f, 0.0f, height);

    for(int i = 0; i < nPolygones; ++i) {
        float angle1 = glm::two_pi<float>() * i / nPolygones;
        glm::vec3 v1 = radius * glm::vec3(glm::cos(angle1), glm::sin(angle1), 0.0f);

        float angle2 = glm::two_pi<float>() * (i + 1) / nPolygones;
        glm::vec3 v2 = radius * glm::vec3(glm::cos(angle2), glm::sin(angle2), 0.0f);

        // боковая поверхность
        vertices.push_back(v1);
        vertices.push_back(v2);
        vertices.push_back(coneTop);

        normals.push_back(glm::normalize(v1));
        normals.push_back(glm::normalize(v2));
        normals.push_back(glm::normalize(coneTop));

        texcoords.push_back(glm::vec2((0.0 + i) / nPolygones, 0));
        texcoords.push_back(glm::vec2((1.0 + i) / nPolygones, 0));
        texcoords.push_back(glm::vec2((0.5 + i) / nPolygones, 10));

        // основание
        vertices.push_back(v1);
        vertices.push_back(v2);
        vertices.emplace_back(glm::vec3(0.0f, 0.0f, 0.0f));

        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));

        texcoords.push_back(glm::vec2(0, 0));
        texcoords.push_back(glm::vec2(0, 0));
        texcoords.push_back(glm::vec2(0, 0));
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Cone is created with " << vertices.size() << " vertices\n";

    return mesh;
}


MeshPtr makeRect(float width, float height)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    //front 1
    vertices.push_back(glm::vec3(-width / 2, height / 2, 0.0));
    vertices.push_back(glm::vec3(width / 2, -height / 2, 0.0));
    vertices.push_back(glm::vec3(width / 2, height / 2, 0.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(0, 1));
    texcoords.push_back(glm::vec2(1, 0));
    texcoords.push_back(glm::vec2(1, 1));

    //front 2
    vertices.push_back(glm::vec3(-width / 2, height / 2, 0.0));
    vertices.push_back(glm::vec3(-width / 2, -height / 2, 0.0));
    vertices.push_back(glm::vec3(width / 2, -height / 2, 0.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(0, 1));
    texcoords.push_back(glm::vec2(0, 0));
    texcoords.push_back(glm::vec2(1, 0));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


class TreeBuilder {
public:

    void Build( std::vector<MeshPtr>& wood, std::vector<MeshPtr>& leafs )
    {
        build( 1, 0.1, 3, glm::translate(glm::mat4( 1.0f ), glm::vec3(0.0f, 0.0f, -1.0f) ), wood, leafs );
    }

private:
    void build( int depth, float radius, float length, glm::mat4 mat, std::vector<MeshPtr>& wood, std::vector<MeshPtr>& leafs )
    {
        if( depth > 3 && depth <= 6 ) {
            int n_leafs = 10 + rand() % 10;
            for( int i = 0; i < n_leafs; ++i ) {
                float width = 0.03;
                float height = 0.05;
                float displacement = length * ( 0.3f + 1.0f * (rand() % 70) / 100);
                float distance = radius * ( 1.0f + (rand() % 200) / 100 );
                float alpha = glm::radians( 1.0f * (rand() % 360) );
                float beta = glm::radians( -30.0f + rand() % 100 );

                glm::mat4 leafMat = glm::rotate( glm::mat4(1), alpha, glm::vec3( 0.0f, 0.0f, 1.0f ) );
                leafMat = glm::translate( leafMat, glm::vec3( distance, 0, displacement ));
                leafMat = glm::rotate( leafMat, beta, glm::vec3( 1.0f, 0.0f, 0.0f ) );
                leafMat = glm::translate( leafMat, glm::vec3( 0, height / 2, 0 ));

                MeshPtr leaf = makeRect(width, height);
                leaf->setModelMatrix(mat * leafMat);
                leafs.push_back(leaf);
            }
        } else if( depth > 5 ) {
            return;
        }

        int n = 100 / (depth * glm::log(1 + depth) );

        MeshPtr cone = makeCone( length, radius, n );
        cone->setModelMatrix( mat );
        wood.push_back( cone );

        for( int level = 1; level <= 3; ++level ) {
            int n_children = 3 + rand() % 3;
            for( int i = 0; i < n_children; ++i ) {
                float h = length * ( 0.15f + 0.2f * level + 1.0f * (rand() % 20) / 100 );
                float r = radius * ( 1.0f - h / length) * (0.9f - 1.0f * (rand() % 20) / 100 );
                float l = length * ( 0.7f - 1.0f * (rand() % 20) / 100 ) / level;
                float alpha = glm::radians( 360.0f * i / n_children + rand() % 40 );
                float beta = glm::radians( 30.0f + rand() % 10 );

                glm::mat4 childMat = glm::translate( glm::mat4( 1.0f ), glm::vec3( 0.0f, 0.0f, h ) );
                childMat = glm::rotate( childMat, alpha, glm::vec3( 0.0f, 0.0f, 1.0f ) );
                childMat = glm::rotate( childMat, beta, glm::vec3( 1.0f, 0.0f, 0.0f ) );

                build( depth + level, r, l, mat * childMat, wood, leafs );
            }
        }
    }
};


class TreeApplication : public Application {
public:
    std::vector<MeshPtr> _wood;
    std::vector<MeshPtr> _leafs;

    ShaderProgramPtr _shader;
    TexturePtr _barkTexture;
    TexturePtr _leafTexture;

    GLuint _sampler;

    //Переменные для управления положением одного источника света
    float _lr = 10.0;
    float _phi = glm::pi<float>() * 0.5f;
    float _theta = glm::pi<float>() * 0.25f;

    LightInfo _light;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        TreeBuilder().Build( _wood, _leafs );

        _shader = std::make_shared<ShaderProgram>("591BelkinData/shader.vert", "591BelkinData/shader.frag");

        _leafTexture = loadTexture("591BelkinData/leaf.png");
        _barkTexture = loadTexture("591BelkinData/bark.jpg");

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.9, 0.9, 0.9);
        _light.specular = glm::vec3(0.05, 0.05, 0.05);

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        glEnable(GL_BLEND);
        glBlendColor(0.7, 0.7, 0.7, 0.7);
        //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glBlendFunc(GL_CONSTANT_COLOR, GL_ONE_MINUS_CONSTANT_COLOR);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _leafTexture->bind();
        _shader->setIntUniform("diffuseTex", 0);

        for( MeshPtr mp : _leafs ) {
            _shader->setMat4Uniform("modelMatrix", mp->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * mp->modelMatrix()))));
            mp->draw();
        }

        glDisable(GL_BLEND);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _barkTexture->bind();
        _shader->setIntUniform("diffuseTex", 0);

        for( MeshPtr mp : _wood ) {
            _shader->setMat4Uniform("modelMatrix", mp->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * mp->modelMatrix()))));
            mp->draw();
        }
    }
};

int main()
{
    srand( 42 );
    TreeApplication app;
    app.start();

    return 0;
}
