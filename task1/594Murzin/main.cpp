#include "./common/Application.hpp"
#include "./common/Mesh.hpp"
#include "./common/ShaderProgram.hpp"

#include "cylinder.h"

#include <iostream>
#include <vector>

const float pi = glm::pi<float>();
const float deg_to_rad = pi / 180;

void generate_cones(std::vector<MeshPtr> &cones, glm::mat4 mat0, size_t level, float height, float radius) {
	if (level == 0) return;

	MeshPtr cone0 = makeCone(height, radius, 100);
	cone0->setModelMatrix(mat0);
	cones.push_back(cone0);

	const size_t number_branches = 4;
	const float branch_tilt = deg_to_rad * 40;
	const float branch_grow = 1;
	for (int i = 0; i < number_branches; ++i) {
		auto f = [&](float a) {
			glm::mat4 mat = mat0;
			mat = glm::translate(mat, glm::vec3(0.0f, 0.0f, height * a));
			float random01 = (rand() / (float) RAND_MAX) / 10;
			mat = glm::rotate(mat, 2 * pi / number_branches * (i + random01), glm::vec3(0.0f, 0.0f, 1.0f));
			mat = glm::rotate(mat, branch_tilt, glm::vec3(1.0f, 0.0f, 0.0f));
			generate_cones(cones, mat, level - 1, height * (1 - a) * branch_grow, radius * (1 - a));
		};

		// f(1.0 / 3);
		// f(2.0 / 3);
		f(0.4);
	}
}

struct SampleApplication : public Application {
	std::vector<MeshPtr> _cones;

	ShaderProgramPtr _shader;

	void makeScene() override {
		Application::makeScene();

		_cameraMover = std::make_shared<FreeCameraMover>();

		generate_cones(_cones, glm::mat4(1), 4, 2, 0.1);

		//Создаем шейдерную программу
		_shader = std::make_shared<ShaderProgram>("594MurzinData1/shaderNormal.vert", "594MurzinData1/shader.frag");
	}

	void draw() override {
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Устанавливаем шейдер.
		_shader->use();

		//Устанавливаем общие юниформ-переменные
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		//Рисуем первый меш
		for (const MeshPtr &cone : _cones) {
			_shader->setMat4Uniform("modelMatrix", cone->modelMatrix());
			cone->draw();
		}
	}
};

int main() {
	// чтобы не создавался файл imgui.ini
	ImGui::GetIO().IniFilename = nullptr;

	SampleApplication app;
	app.start();

	return 0;
}
