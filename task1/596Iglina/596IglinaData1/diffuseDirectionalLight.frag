#version 330

// in vec3 color; //интерполированный цвет между вершинами треугольника
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

in float color;
out vec4 fragColor; //выходной цвет фрагмента

uniform sampler2D diffuseTex;
uniform int isPolygon;

void main()
{//texture(diffuseTex, texCoord).rgb
    if (isPolygon == 0) {
        fragColor = vec4(0.3, color, 0.3, 1.0); //просто копируем
    }
    else {
        fragColor = vec4(0.0, 0.0, 0.0, 1.0);
    }
}
